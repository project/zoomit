<?php

/**
 * @file
 * Helper functions to invoke Zoom.It's API and download the content.
 */

/**
 * Call the Zoom.It API, and download the tiles.
 *
 * @param Mixed $image If this is a STRING:
 *                     - $image should be a filepath to the image file.
 *                     If this is an OBJECT:
 *                     - $image should be a Drupal file object, or at least
 *                       provide a 'filepath' property.
 *
 * @return void
 */
function zoomit_call($image) {
  // parse our image into a URL.
  if (is_string($image)) {
    $filepath = $image;
  }
  else if (is_object($image)) {
    $filepath = $image->filepath;
  }
  else {
    return null;
  }

  $url = $filepath;
  $url = ZOOMIT_API_URL . '?url=' . urlencode($url);
  $result = drupal_http_request($url, array('Accept' => 'application/json'));
  if($result) {
    // The DZI defines tile-sizes, etc.
    $result = json_decode($result->data);
    // take the embed script given by zoomit.
    $embed = $result->embedHtml;
    
    // add it to our cache
    $key = base64_encode($filepath);
    $ids = variable_get('zoomit_processed', array());
    $ids[$key] = $embed;
    variable_set('zoomit_processed', $ids);
    
    return $embed;
    
  }
}


/**
 * Get the embed-code for a given URL.
 */
function zoomit_embed($url) {
  return zoomit_call($url);
}


