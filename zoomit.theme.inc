<?php

/**
 * @file
 * Theme handlers for the zoomit module.
 */

/**
 * CCK field formatter
 */
function theme_zoomit_formatter_zoomit($element) {
  $field = content_fields($element['#field_name']);
  $item = $element['#item'];
  
  $src = $item['filepath'];
  if(empty($src) || !file_exists($src))
  {
    return '';
  }
  
  // TODO: how should alt and title attributes be used?
  $class = 'zoomit zoomit-'. $field['field_name'];
  return theme('zoomit_local_image', $item, $item['data']['alt'], $item['data']['title'], array('class' => $class));
}


/**
 * Handler for theme('zoomit')
 *
 * @param Mixed $image If this is a STRING:
 *                     - $image should be a filepath to the image file.
 *                     If this is an OBJECT:
 *                     - $image should be a Drupal file object, or at least
 *                       provide a 'filepath' property.
 *
 * @return String
 * Embed-code to embed the zoomable image on the page.
 */
function theme_zoomit_local_image($file, $alt = '', $title = '', $attributes = NULL) {
  $file = (array) $file;
  
  // Encode the parts of the path.
  $parts = explode('/', $file['filepath']);
  foreach ($parts as $n => $part) {
    $parts[$n] = rawurlencode($part);
  }
  $path = implode('/', $parts);
  $url = file_create_url($path) . $query_string;

  return theme('zoomit_image', $url, $alt, $title, $attributes);
}


function theme_zoomit_image($url, $alt = '', $title = '', $attributes = NULL) {

  if (!empty($title)) {
    $attributes['title'] = $title;
  }

  $attributes = drupal_attributes($attributes);
  
  // TODO: add imagecache support as an alternative fallback.
  // $fallback_code = '<noscript>';
  // $fallback_code .=  theme('image', $file['filepath'], $alt, $title, $attributes);
  // $fallback_code .= '</noscript>';
  
  module_load_include('inc', 'zoomit');
  $embed_code = zoomit_embed($url);

  $output = '<div ' . $attributes . '>';
  $output .= $embed_code;
  $output .= $fallback_code;
  $output .= '</div>';
  
  return $output;
}
